process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
let index = require('./index');
let should = chai.should();

chai.use(chaiHttp);


describe('shows', () => {
  describe('/POST root', () => {
      it('it should return payload error', (done) => {
        let show = {}
        chai.request(index)
            .post('/')
            .send(show)
            .end((err, res) => {
                  res.should.have.status(400);
                  res.body.should.be.a('object');
                  res.body.should.have.property('error');
                  res.body.error.should.to.equal('payload is empty');                  
              done();
            });
      });

      it('it should return empty response', (done) => {
        let show = {"payload":[]}
        chai.request(index)
            .post('/')
            .send(show)
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('object');
                  res.body.should.have.property('response');
                  res.body.response.should.to.eql([]);                  
              done();
            });
      });

      it('it should return valid response', (done) => {
        let show = {"payload":[{
            "drm": true,
            "episodeCount": 3,
            "image": {
                "showImage": "image1"
            },
            "slug": "slug1",
            "title": "show1",
        },
        {
            "drm": true,
            "episodeCount": 1,
            "image": {
                "showImage": "image2"
            },
            "slug": "slug2",
            "title": "show2",
        }
        ]}
        chai.request(index)
            .post('/')
            .send(show)
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('object');
                  res.body.should.have.property('response');
                  res.body.response.should.to.eql([
                    {
                      image: "image1",
                      slug: "slug1",
                      title: "show1"
                    },
                    {
                      image: "image2",
                      slug: "slug2",
                      title: "show2"
                    }
                  ]);                  
              done();
            });
      });

      it('it should return filtered response', (done) => {
        let show = {"payload":[{
            "drm": true,
            "episodeCount": 0,
            "image": {
                "showImage": "image1"
            },
            "slug": "slug1",
            "title": "show1",
        },
        {
            "drm": false,
            "episodeCount": 1,
            "image": {
                "showImage": "image2"
            },
            "slug": "slug2",
            "title": "show2",
        },
        {
            "drm": true,
            "episodeCount": 1,
            "image": {
                "showImage": "image3"
            },
            "slug": "slug3",
            "title": "show3",
        }
        ]}
        chai.request(index)
            .post('/')
            .send(show)
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('object');
                  res.body.should.have.property('response');
                  res.body.response.should.to.eql([
                    {
                      image: "image3",
                      slug: "slug3",
                      title: "show3"
                    }
                  ]);                  
              done();
            });
      });
    
      it('it should return empty fields', (done) => {
        let show = {"payload":[{
            "drm": true,
            "episodeCount": 3,            
        },
        {
            "drm": true,
            "episodeCount": 1,
            "image": {
                "showImage": "image2"
            },
            "slug": "slug2",
            "title": "show2",
        }
        ]}
        chai.request(index)
            .post('/')
            .send(show)
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('object');
                  res.body.should.have.property('response');
                  res.body.response.should.to.eql([
                    {
                      image: "no Image",
                      slug: "no slug",
                      title: "no title"
                    },
                    {
                      image: "image2",
                      slug: "slug2",
                      title: "show2"
                    }
                  ]);                  
              done();
            });
      });
  });
});