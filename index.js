const express = require('express')
const app = express()
const PORT = process.env.PORT || 8080


app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.get('/', (req, res) => {
  res.send('Welcome to C9 Test')
})

/**
 * This function validates and returns back shows.
 */
app.post('/', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  const request = req.body;
  if (!request.payload){
    res.status(400).end(JSON.stringify({ "error": "payload is empty"}));   
    return;
  }
  const shows = request.payload.filter((show)=>{
    return show.drm && show.episodeCount>0
  });
  const response = shows.map((show)=>{
    return {
      "image": show.image?.showImage ?? "no Image",
      "title": show?.title?? "no title",
      "slug": show?.slug?? "no slug",
    };
  });
  res.end(JSON.stringify({response}));
})

/**
 * This function validates that the json is valid
 */
app.use((err, req, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
      return res.status(400).send(JSON.stringify({
        "error": "Could not decode request: JSON parsing failed" 
      })); // Bad request
  }
  next();
});

app.listen(PORT, () => {
  console.log(`app listening at http://localhost:${PORT}`)
})

module.exports = app; // for testing